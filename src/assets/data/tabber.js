const tabbarData = [
  {
    text: '首页',
    image: 'tabber/home.png',
    imageActive: 'tabber/home-active.png',
    path: '/home',
  },
  {
    text: '收藏',
    image: 'tabber/favor.png',
    imageActive: 'tabber/favor-active.png',
    path: '/favor',
  },
  {
    text: '订单',
    image: 'tabber/order.png',
    imageActive: 'tabber/order-active.png',
    path: '/order',
  },
  {
    text: '消息',
    image: 'tabber/message.png',
    imageActive: 'tabber/message-active.png',
    path: '/message',
  },
]

export default tabbarData