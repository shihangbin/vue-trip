const bannerData = [
  {
    image: 'banner/01.png',
    url: 'http://172.20.10.4:51011/#/home'
  },
  {
    image: 'banner/02.png',
    url: 'http://172.20.10.4:51011/#/favor'
  },
  {
    image: 'banner/03.png',
    url: 'http://172.20.10.4:51011/#/order'
  },
  {
    image: 'banner/04.png',
    url: 'http://172.20.10.4:51011/#/message'
  },
  {
    image: 'banner/05.png',
    url: 'http://172.20.10.4:51011/#/city'
  },
]

export default bannerData