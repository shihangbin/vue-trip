import { defineStore } from 'pinia'

export const useLodingStore = defineStore('loading', {
  state: () => {
    return {
      isLoading: false
    }
  }
})