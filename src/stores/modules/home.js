import { defineStore } from 'pinia'
import { getHomeHotSuggests, getCategoties, gethouselist } from '@/service'

export const useHomeStore = defineStore('home', {
  state: () => ({
    hotSuggests: [],
    categoties: [],
    currentPage: 1,
    houselist: [],
    date: {
      startDate: new Date,
      endDate: new Date
    }
  }),
  actions: {
    async hotSuggestsAction() {
      const res = await getHomeHotSuggests()
      this.hotSuggests = res.data
    },
    async categotiesAction() {
      const res = await getCategoties()
      this.categoties = res.data
    },
    async houselistAction() {
      const res = await gethouselist(this.currentPage)
      // 把数组中的数据展开放入原数组
      this.houselist.push(...res.data)
      this.currentPage++
    }
  }
})