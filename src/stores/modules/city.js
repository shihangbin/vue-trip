// 2. 管理网络请求 => city
import { defineStore } from "pinia";
import { getCityAll } from '@/service'

export const useCityStore = defineStore('city', {
  state: () => ({
    // 热门城市数据|列表
    allcities: {},
    // 选中城市数据
    currentCity: {
      cityName: '大理'
    }
  }),
  actions: {
    async fetchAllCitiesData() {
      const res = await getCityAll()
      this.allcities = res.data
    }
  }
})
