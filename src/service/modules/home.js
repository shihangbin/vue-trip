import XBRequest from '@/service/request/index'

export function getHomeHotSuggests() {
  return XBRequest.get({ url: '/home/hotSuggests' })
}

export function getCategoties() {
  return XBRequest.get({ url: '/home/categories' })
}

export function gethouselist(currentPage) {
  return XBRequest.get({
    url: '/home/houselist',
    params: {
      page: currentPage
    }
  })
}