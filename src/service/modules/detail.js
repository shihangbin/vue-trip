import XBRequest from '@/service/request/index'

export function getDetailInfos(houseId) {
  return XBRequest.get({
    url: '/detail/infos',
    params: {
      houseId
    }
  })
}